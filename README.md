# Flows Kanji Colorizer #

This is an extension of the Kanji Colorizer and NHK-Pronounciation plugins for Anki.
The files go in the plugin folder.

It depends on the following plugins:
* Kanji Colorizer (original)
* NHK-Pronounciation
* Japanese Support